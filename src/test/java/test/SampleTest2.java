package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SampleTest2 {

	@DataProvider(name = "excelData")
	public Object[][] readExcel() throws IOException {
		File file = new File("C:\\Project\\api-test\\src\\test\\java\\test\\test2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook workbook = new XSSFWorkbook(fis);
		XSSFSheet sheet = workbook.getSheet("login");
		int totalRows = sheet.getLastRowNum();
		int totalColums = sheet.getRow(0).getPhysicalNumberOfCells();

		// Read data from excel and store the same in the Object Array.
		Object obj[][] = new Object[totalRows][totalColums];
		for (int i = 0; i < totalRows; i++) {
			obj[i][0] = sheet.getRow(i + 1).getCell(0).toString();
			obj[i][1] = sheet.getRow(i + 1).getCell(1).toString();
		}

		return obj;
	}

	@Test(dataProvider = "excelData")
	public void validateUser(String userName, String password) throws InterruptedException {
		Reporter.log(userName);
		Reporter.log(password);
	}

}
