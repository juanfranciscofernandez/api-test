package test.profile;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class ProfileControllerTest {

	private String PROFILE = "http://localhost:8584/api/public/v1/profile/check";

	@AfterMethod
	public void afterMethod(ITestResult iTestResult) throws IOException {
		Reporter.log("After Method: " + iTestResult.getMethod().getMethodName());
	}

	@Test(groups = { "profile" })

	public void checkProfile() {
		Reporter.log("Retreive profile");

		// Getting the RequestSpecification of the request

		RequestSpecification httpRequest = RestAssured.given();

		// Making GET request directly by RequestSpecification.get() method

		Response response = httpRequest.get(PROFILE);

		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();
		int statusCode = response.getStatusCode();

		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");

		Reporter.log("The content body : " + bodyStringValue);

		Reporter.log("The status code is : " + statusCode);
	}
}
