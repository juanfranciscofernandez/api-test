package test.timezones;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class TimeZonesControllerTest {

	private String TIMEZONES = "http://localhost:8584/api/public/timeZones";

	@AfterMethod
	public void afterMethod(ITestResult iTestResult) throws IOException {
		Reporter.log("After Method: " + iTestResult.getMethod().getMethodName());
	}

	@Test(groups = { "timezones" })

	public void getTimeZones() {
		Reporter.log("Retreive all timezones");

		// Getting the RequestSpecification of the request

		RequestSpecification httpRequest = RestAssured.given();

		// Making GET request directly by RequestSpecification.get() method

		Response response = httpRequest.get(TIMEZONES);

		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();
		int statusCode = response.getStatusCode();

		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");

		Reporter.log("The content body : " + bodyStringValue);

		Reporter.log("The status code is : " + statusCode);
	}

}
