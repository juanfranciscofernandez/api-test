package test.countries;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class CountriesControllerTest {

	private String COUNTRIES = "http://localhost:8584/api/public/v1/countries";
	private String RETREIVECOUNTRY = "http://localhost:8584/api/public/v1/countries/1";
	private String RETREIVECOUNTRYBYALPHACODE2 = "http://localhost:8584/api/public/v1/country/alpha2Code/ES";
	private String RETREIVECOUNTRYBYALPHACODE3 = "http://localhost:8584/api/public/v1/country/alpha3Code/ESP";

	@AfterMethod
	public void afterMethod(ITestResult iTestResult) throws IOException {
		Reporter.log("After Method: " + iTestResult.getMethod().getMethodName());
	}

	@Test(groups = { "countries" })
	public void getCountries() {
		Reporter.log("Retreive all countries");
		
		// Getting the RequestSpecification of the request
		
		RequestSpecification httpRequest = RestAssured.given();
		
		// Making GET request directly by RequestSpecification.get() method
		
		Response response = httpRequest.get(COUNTRIES);		
		
		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();	  	
		int statusCode = response.getStatusCode();
		
		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");
				
		Reporter.log("The content body : " + bodyStringValue);
		
		Reporter.log("The status code is : " + statusCode);
	}
	
	@Test(groups = { "countries" })
	public void getCountryById() {
		Reporter.log("Retreive country by Id");
		
		// Getting the RequestSpecification of the request
		
		RequestSpecification httpRequest = RestAssured.given();
		
		// Making GET request directly by RequestSpecification.get() method
		
		Response response = httpRequest.get(RETREIVECOUNTRY);		
		
		
		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();	  	
		
		//Status code
		int statusCode = response.getStatusCode();
		
		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");
		
		Reporter.log("The content body : " + bodyStringValue);
		
		Reporter.log("The status code is : " + statusCode);
	}
	
	@Test(groups = { "countries" })
	public void getCountryByAlphaCode2() {
		Reporter.log("Retreive country by alphaCode2");
		
		// Getting the RequestSpecification of the request
		
		RequestSpecification httpRequest = RestAssured.given();
		
		// Making GET request directly by RequestSpecification.get() method
		
		Response response = httpRequest.get(RETREIVECOUNTRYBYALPHACODE2);		
		
		//Print body
		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();	  	
		
		//Status code
		int statusCode = response.getStatusCode();
		
		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");
		
		Reporter.log("The content body : " + bodyStringValue);
		
		Reporter.log("The status code is : " + statusCode);
	}
	
	@Test(groups = { "countries" })
	public void getCountryByAlphaCode3() {
		Reporter.log("Retreive country by alphaCode3");
		
		// Getting the RequestSpecification of the request
		
		RequestSpecification httpRequest = RestAssured.given();
		
		// Making GET request directly by RequestSpecification.get() method
		
		Response response = httpRequest.get(RETREIVECOUNTRYBYALPHACODE3);		
		
		//Print body
		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();	  	
		
		//Status code
		int statusCode = response.getStatusCode();
		
		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");
		
		Reporter.log("The content body : " + bodyStringValue);
		
		Reporter.log("The status code is : " + statusCode);
	}

}
