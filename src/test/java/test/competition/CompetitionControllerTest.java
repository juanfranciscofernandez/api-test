package test.competition;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class CompetitionControllerTest {

	private String COMPETICIONES = "http://localhost:8584/api/public/league/sports/1";

	@Test(groups = { "competiciones" })
	public void getCompeticiones() {

		Reporter.log("Retreive all competitons");
		
		// Getting the RequestSpecification of the request

		RequestSpecification httpRequest = RestAssured.given();

		// Making GET request directly by RequestSpecification.get() method

		Response response = httpRequest.get(COMPETICIONES);

		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();
		int statusCode = response.getStatusCode();

		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");

		Reporter.log("The content body : " + bodyStringValue);

		Reporter.log("The status code is : " + statusCode);

	}

}
