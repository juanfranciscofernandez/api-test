package test.sports;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class SportsControllerTest {

	private String SPORTS = "http://localhost:8584/api/public/v1/sports";
	private String RETREIVESPORTBYID = "http://localhost:8584/api/public/v1/sport/4/language/1";

	@AfterMethod
	public void afterMethod(ITestResult iTestResult) throws IOException {
		Reporter.log("After Method: " + iTestResult.getMethod().getMethodName());
	}

	@Test(groups = { "sports" })

	public void getSports() {
		Reporter.log("Retreive all sports");

		// Getting the RequestSpecification of the request

		RequestSpecification httpRequest = RestAssured.given();

		// Making GET request directly by RequestSpecification.get() method

		Response response = httpRequest.get(SPORTS);

		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();
		int statusCode = response.getStatusCode();

		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");
		
		Reporter.log("The content body : " + bodyStringValue);

		Reporter.log("The status code is : " + statusCode);
	}

	@Test(groups = { "sports" })
	public void getSportById() {
		Reporter.log("Retreive sport by id");

		// Getting the RequestSpecification of the request

		RequestSpecification httpRequest = RestAssured.given();

		// Making GET request directly by RequestSpecification.get() method

		Response response = httpRequest.get(RETREIVESPORTBYID);

		// Print body
		ResponseBody body = response.getBody();
		String bodyStringValue = body.asString();

		// Status code
		int statusCode = response.getStatusCode();

		Assert.assertEquals(statusCode /* actual value */, 200 /* expected value */, "Correct status code returned");

		Reporter.log("The content body : " + bodyStringValue);

		Reporter.log("The status code is : " + statusCode);
	}

}
